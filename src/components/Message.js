import React from 'react';

function Message(props) {
  let css = props.message.local ? 'box sent' : 'box received' 
  return (
    <div className={css}>
      {props.message.text}
    </div>  
  )
}

export default Message;

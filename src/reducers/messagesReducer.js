import { GET_MESSAGES, ADD_MESSAGE } from "../types/index";
import initialState from "./initialState";

const messagesReducer = (state = initialState.messages, action) => {
  switch (action.type) {
    case GET_MESSAGES:
      return { ...state, messages: action.payload };
    case ADD_MESSAGE:
      return { ...state, messages: [...state.messages, action.payload] };
    default:
      return state;
  }
};

export default messagesReducer;
